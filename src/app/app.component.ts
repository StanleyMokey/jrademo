import { Component, ViewChild } from '@angular/core';
import { Platform, Nav } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { NewIssuePage } from '../pages/new-issue/new-issue';
import { MyLoggedIssuesPage } from '../pages/my-logged-issues/my-logged-issues';
import { ManageWatchlistsPage } from '../pages/manage-watchlists/manage-watchlists';
import { NewflashHistoryPage } from '../pages/newflash-history/newflash-history';
import { AboutPage } from '../pages/about/about';
import { NotificationsPage } from '../pages/notifications/notifications';
import { CityRegionsPage } from '../pages/city-regions/city-regions';
import { JRAContactDetailsPage } from '../pages/j-racontact-details/j-racontact-details';
import { LogANewIssuePage } from '../pages/log-anew-issue/log-anew-issue';



@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) navCtrl: Nav;
    rootPage:any = CityRegionsPage;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }
  goToLogANewIssue(params){
    if (!params) params = {};
    this.navCtrl.setRoot(LogANewIssuePage);
  }goToNewIssue(params){
    if (!params) params = {};
    this.navCtrl.setRoot(NewIssuePage);
  }goToMyLoggedIssues(params){
    if (!params) params = {};
    this.navCtrl.setRoot(MyLoggedIssuesPage);
  }goToManageWatchlists(params){
    if (!params) params = {};
    this.navCtrl.setRoot(ManageWatchlistsPage);
  }goToNewflashHistory(params){
    if (!params) params = {};
    this.navCtrl.setRoot(NewflashHistoryPage);
  }goToAbout(params){
    if (!params) params = {};
    this.navCtrl.setRoot(AboutPage);
  }goToNotifications(params){
    if (!params) params = {};
    this.navCtrl.setRoot(NotificationsPage);
  }goToCityRegions(params){
    if (!params) params = {};
    this.navCtrl.setRoot(CityRegionsPage);
  }goToJRAContactDetails(params){
    if (!params) params = {};
    this.navCtrl.setRoot(JRAContactDetailsPage);
  }
}
