import { NgModule, ErrorHandler, Component } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { LogANewIssuePage } from '../pages/log-anew-issue/log-anew-issue';
import { MyLoggedIssuesPage } from '../pages/my-logged-issues/my-logged-issues';
import { ManageWatchlistsPage } from '../pages/manage-watchlists/manage-watchlists';
import { TabsControllerPage } from '../pages/tabs-controller/tabs-controller';
import { NewflashHistoryPage } from '../pages/newflash-history/newflash-history';
import { JRAContactDetailsPage } from '../pages/j-racontact-details/j-racontact-details';
import { CityRegionsPage } from '../pages/city-regions/city-regions';
import { NotificationsPage } from '../pages/notifications/notifications';
import { AboutPage } from '../pages/about/about';
import { NewIssuePage } from '../pages/new-issue/new-issue';
import { CurrentLocationPage } from '../pages/current-location/current-location';
import { AgmCoreModule } from '@agm/core';
import { Camera } from '@ionic-native/camera';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Geolocation ,GeolocationOptions ,Geoposition ,PositionError } from '@ionic-native/geolocation';

@Component({
  selector: 'app-root',
  styles: [`agm-map {height: 300px;}`],
  template: `<agm-map [latitude]="lat" [longitude]="lng"></agm-map>`
})
export class AppComponent {
  lat: number = 51.678418;
  lng: number = 7.809007;
}
@NgModule({
  declarations: [
    MyApp,
    LogANewIssuePage,
    MyLoggedIssuesPage,
    ManageWatchlistsPage,
    TabsControllerPage,
    NewflashHistoryPage,
    JRAContactDetailsPage,
    CityRegionsPage,
    NotificationsPage,
    AboutPage,
    NewIssuePage,
    CurrentLocationPage,
    AppComponent
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyBu-916DdpKAjTmJNIgngS6HL_kDIKU0aU'
    })
    
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    LogANewIssuePage,
    MyLoggedIssuesPage,
    ManageWatchlistsPage,
    TabsControllerPage,
    NewflashHistoryPage,
    JRAContactDetailsPage,
    CityRegionsPage,
    NotificationsPage,
    AboutPage,
    NewIssuePage,
    CurrentLocationPage,
    AppComponent
    
  ],
  providers: [
    Camera,
    Geolocation,
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}