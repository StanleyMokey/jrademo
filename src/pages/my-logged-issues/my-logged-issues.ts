import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { NewIssuePage } from '../new-issue/new-issue';

@Component({
  selector: 'page-my-logged-issues',
  templateUrl: 'my-logged-issues.html'
})
export class MyLoggedIssuesPage {

  constructor(public navCtrl: NavController) {
  }
  goToNewIssue(params){
    if (!params) params = {};
    this.navCtrl.push(NewIssuePage);
  }
}
