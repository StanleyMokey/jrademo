import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { NewIssuePage } from '../new-issue/new-issue';

@Component({
  selector: 'page-notifications',
  templateUrl: 'notifications.html'
})
export class NotificationsPage {

  constructor(public navCtrl: NavController) {
  }
  goToNewIssue(params){
    if (!params) params = {};
    this.navCtrl.push(NewIssuePage);
  }
}
