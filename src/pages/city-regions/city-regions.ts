import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { NewflashHistoryPage } from '../newflash-history/newflash-history';

@Component({
  selector: 'page-city-regions',
  templateUrl: 'city-regions.html'
})
export class CityRegionsPage {
	
  constructor(public navCtrl: NavController) {
  }
  goHome(){
        this.navCtrl.push(NewflashHistoryPage);
   }
}
