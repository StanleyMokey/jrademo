import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-current-location',
  templateUrl: 'current-location.html'
})
export class CurrentLocationPage {

  constructor(public navCtrl: NavController) {
  }
  
}
