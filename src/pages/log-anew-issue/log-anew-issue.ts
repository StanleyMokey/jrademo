import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { NewIssuePage } from '../new-issue/new-issue';

@Component({
  selector: 'page-log-anew-issue',
  templateUrl: 'log-anew-issue.html'
})
export class LogANewIssuePage {

  constructor(public navCtrl: NavController) {
  }
  goToNewIssue(params){
    if (!params) params = {};
    this.navCtrl.push(NewIssuePage);
  }
}
