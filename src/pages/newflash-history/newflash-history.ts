import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { LogANewIssuePage } from '../log-anew-issue/log-anew-issue';
import { AboutPage } from '../about/about';

@Component({
  selector: 'page-newflash-history',
  templateUrl: 'newflash-history.html'
})
export class NewflashHistoryPage {

  constructor(public navCtrl: NavController) {
  }
  goNewIssue(){
        this.navCtrl.push(LogANewIssuePage);
  }
  goAbout(){
        this.navCtrl.push(AboutPage);
  }



}
