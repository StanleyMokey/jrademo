import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { LogANewIssuePage } from '../log-anew-issue/log-anew-issue';
import { MyLoggedIssuesPage } from '../my-logged-issues/my-logged-issues';
import { ManageWatchlistsPage } from '../manage-watchlists/manage-watchlists';

@Component({
  selector: 'page-tabs-controller',
  templateUrl: 'tabs-controller.html'
})
export class TabsControllerPage {

  tab1Root: any = LogANewIssuePage;
  tab2Root: any = MyLoggedIssuesPage;
  tab3Root: any = ManageWatchlistsPage;
  constructor(public navCtrl: NavController) {
  }
  
}
