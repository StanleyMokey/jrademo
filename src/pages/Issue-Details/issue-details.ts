import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';


@Component({
  selector: 'page-issue-details',
  templateUrl: 'Issue-Details.html'
})
export class IssueDetailsPage {

  constructor(public navCtrl: NavController) {
  }
  
}
